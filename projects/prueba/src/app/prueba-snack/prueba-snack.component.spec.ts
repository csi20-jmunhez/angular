import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PruebaSnackComponent } from './prueba-snack.component';

describe('PruebaSnackComponent', () => {
  let component: PruebaSnackComponent;
  let fixture: ComponentFixture<PruebaSnackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PruebaSnackComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PruebaSnackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
