import { Injectable } from '@angular/core';
import {Equipo} from "./equipo";
import {GRUPOA, GRUPOB, GRUPOC} from "./mocks/mockGrupos";

@Injectable({
  providedIn: 'root'
})
export class BuscaGrupoService {

  equipo:Equipo[] = [];

  constructor() { }

  getGrupo(grupo:string):Equipo[]{

   switch (grupo) {
     case 'A':
       this.equipo = GRUPOA;
       break;

     case 'B':
       this.equipo = GRUPOB;
       break;

     case 'C':
       this.equipo = GRUPOC;
       break;
    }
    return this.equipo;
  }

}
