import {Equipo} from "../equipo";

export const GRUPOA:Equipo[] = [

  {
    id:1,
    nombre:'Mentes Maestras',
    curso:'TSEAS - 1'
  },

  {
    id:2,
    nombre:'Bulgaros',
    curso:'TSEAS - 1'
  },

  {
    id:3,
    nombre:'CMI2',
    curso:'CMI-2'
  },

  {
    id:4,
    nombre:'La Once',
    curso:'CMGA-1'
  }
  ];

export const GRUPOB:Equipo[] = [

  {
    id:1,
    nombre:'Los Estudiantes',
    curso:'TSEAS - 1'
  },

  {
    id:2,
    nombre:'Manchester',
    curso:'TSEAS - 1'
  },

  {
    id:3,
    nombre:'Recreativo',
    curso:'CMI-2'
  },

  {
    id:4,
    nombre:'CMA',
    curso:'CMGA-1'
  }
];

  export const GRUPOC:Equipo[] = [

  {
    id:1,
    nombre:'Ayesa',
    curso:'TSEAS - 1'
  },

  {
    id:2,
    nombre:'CSI1',
    curso:'TSEAS - 1'
  },

  {
    id:3,
    nombre:'Los Enchufados',
    curso:'CMI-2'
  },

  {
    id:4,
    nombre:'CSI2',
    curso:'CMGA-1'
  }
];
