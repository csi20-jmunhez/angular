import { Component, OnInit } from '@angular/core';
import {BuscaGrupoService} from "../busca-grupo.service";
import {Equipo} from "../equipo";
import {ActivatedRoute} from "@angular/router";
import {Location} from "@angular/common";

@Component({
  selector: 'app-listado',
  templateUrl: './listado.component.html',
  styleUrls: ['./listado.component.css']
})
export class ListadoComponent implements OnInit {

  listaEquipo:Equipo[] = [];

  constructor(private grupoServ:BuscaGrupoService,
              private route:ActivatedRoute,
              private location:Location) { }

  ngOnInit(): void {
    let grupo = this.route.snapshot.params.grupo;
    this.listaEquipo = this.grupoServ.getGrupo(grupo);
  }

  goBack():void{
    this.location.back();
  }

}
