import { Component, OnInit } from '@angular/core';
import {Videojuegos} from "../../mock";
import {ActivatedRoute, ParamMap} from "@angular/router";
import {LIBROS} from "../../../../../ruta-child/src/app/mocks";

@Component({
  selector: 'app-videojuego-detail',
  templateUrl: './videojuego-detail.component.html',
  styleUrls: ['./videojuego-detail.component.css']
})
export class VideojuegoDetailComponent implements OnInit {

  videojuego:any;

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let id = parseInt(<string>params.get('id'));
      this.videojuego = Videojuegos.find(item => item.ranking === id);
    });
  }
}
