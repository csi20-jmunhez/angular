import { Component, OnInit } from '@angular/core';
import {Jugadores} from "../../mock";

@Component({
  selector: 'app-lista-jugadores',
  templateUrl: './lista-jugadores.component.html',
  styleUrls: ['./lista-jugadores.component.css']
})
export class ListaJugadoresComponent implements OnInit {

  jugadores:any[] = [];

  constructor() { }

  ngOnInit(): void {
    this.jugadores = Jugadores;
    this.jugadores.sort(function(a,b) {
      return (b.puntuacion - a.puntuacion)
    });
  }

}
