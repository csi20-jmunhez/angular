import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListaJugadoresComponent} from "./lista-jugadores.component";
import {JugadoresRoutingModule} from "./jugadores-routing.module";



@NgModule({
  declarations: [
    ListaJugadoresComponent,
  ],
  imports: [
    CommonModule,
    JugadoresRoutingModule
  ]
})
export class JugadoresModule { }
