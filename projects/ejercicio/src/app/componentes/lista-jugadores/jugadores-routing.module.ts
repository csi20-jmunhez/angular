import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ListaJugadoresComponent} from "./lista-jugadores.component";

const route:Routes = [
  { path: "", component: ListaJugadoresComponent}
];

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports:[RouterModule]
})
export class JugadoresRoutingModule { }
