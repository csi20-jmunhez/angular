import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ListaVideojuegoComponent} from "./lista-videojuego.component";
import {VideojuegoRoutingModule} from "./videojuego-routing.module";
import {VideojuegoDetailComponent} from "../videojuego-detail/videojuego-detail.component";



@NgModule({
  declarations: [
    ListaVideojuegoComponent,
    VideojuegoDetailComponent
  ],
    imports: [
      CommonModule,
      VideojuegoRoutingModule
    ]
})
export class VideojuegoModule { }
