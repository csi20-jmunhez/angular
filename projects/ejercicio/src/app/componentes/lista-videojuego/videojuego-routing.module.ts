import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {ListaVideojuegoComponent} from "./lista-videojuego.component";
import {VideojuegoDetailComponent} from "../videojuego-detail/videojuego-detail.component";

const route:Routes = [
  { path: "", component: ListaVideojuegoComponent},
  { path: "details/:id", component: VideojuegoDetailComponent}
]

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class VideojuegoRoutingModule { }
