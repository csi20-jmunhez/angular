import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaVideojuegoComponent } from './lista-videojuego.component';

describe('ListaVideojuegoComponent', () => {
  let component: ListaVideojuegoComponent;
  let fixture: ComponentFixture<ListaVideojuegoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaVideojuegoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaVideojuegoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
