import { Component, OnInit } from '@angular/core';
import {Videojuegos} from "../../mock";

@Component({
  selector: 'app-lista-videojuego',
  templateUrl: './lista-videojuego.component.html',
  styleUrls: ['./lista-videojuego.component.css']
})
export class ListaVideojuegoComponent implements OnInit {

  videojuegos:any[] = [];

  constructor() { }

  ngOnInit(): void {
    this.videojuegos = Videojuegos;
    this.videojuegos.sort(function(a,b) {
      return (a.ranking - b.ranking)
    });
  }

}
