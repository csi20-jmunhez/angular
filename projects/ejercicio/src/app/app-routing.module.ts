import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./componentes/home/home.component";

const routes: Routes = [
  { path: "home", component: HomeComponent},
  { path: "videojuegos", loadChildren: () => import('./componentes/lista-videojuego/videojuego.module')
      .then(m => m.VideojuegoModule)},
  { path: "usuarios", loadChildren: () => import('./componentes/lista-jugadores/jugadores.module')
      .then(m => m.JugadoresModule)},
  { path: "", redirectTo: "home", pathMatch: "full"},
  { path: "**", redirectTo: "home"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
