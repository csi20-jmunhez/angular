
export const Videojuegos = [
  {

  nombre: "Persona 5: Royal",
  editor: "ATLUS",
  ranking: 1,
  caratula: "assets/persona-5-royal-20202211626933_1b.jpg"
  },
  {
    nombre: "Red Dead Redemption 2",
    editor: "Rockstar",
    ranking: 2,
    caratula: "assets/red-dead-redemption-2-20185218474_1b.jpg"
  },
  {
    nombre: "Half-Life: Alyx",
    editor: "Valve",
    ranking: 3,
    caratula: "assets/halflife-alyx-201911211923616_10b.jpg"
  },
  {
    nombre: "The Legend of Zelda: Breath of the Wild",
    editor: "Nintendo",
    ranking: 4,
    caratula: "assets/the-legend-of-zelda-breath-of-the-wild-201732131429_1b.jpg"
  }
];

export const Jugadores = [
  {

    nombre: "Ana López",
    nick: "Anlo",
    puntuacion: 250,
    foto: "assets/persona2.jpg"
  },
  {
    nombre: "Eric García",
    nick: "Eriko",
    puntuacion: 450,
    foto: "assets/persona1.jpg"
  },
  {
    nombre: "Hugo Sánchez",
    nick: "Hugito",
    puntuacion: 100,
    foto: "assets/persona3.jpg"
  },
  {
    nombre: "Juan Cuesta",
    nick: "Nintendo",
    puntuacion: 25,
    foto: "assets/persona4.jpg"
  }
];
