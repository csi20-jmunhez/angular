import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {VideojuegoModule} from "./componentes/lista-videojuego/videojuego.module";
import {JugadoresModule} from "./componentes/lista-jugadores/jugadores.module";
import { HomeComponent } from './componentes/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    VideojuegoModule,
    JugadoresModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
