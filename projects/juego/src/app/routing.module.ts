import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PptComponent} from "./ppt/ppt.component";
import {AppComponent} from "./app.component";
import {TresRComponent} from "./tres-r/tres-r.component";


const route:Routes = [
  { path:'ppt', component: PptComponent },
  { path:'tresR', component: TresRComponent },

]

@NgModule({
  declarations: [],

  imports: [
    RouterModule.forRoot(route)
  ],

  exports:[
    RouterModule
  ]
})
export class RoutingModule { }
