import { Component, OnInit } from '@angular/core';
import {GameService} from "../game.service";

@Component({
  selector: 'app-tres-r',
  templateUrl: './tres-r.component.html',
  styleUrls: ['./tres-r.component.css']
})
export class TresRComponent implements OnInit {

  constructor( public gameService: GameService){

  }

  resetGame(){
    this.gameService.newGame()
  }

  ngOnInit(): void {
    this.gameService.getBoard;
  }

}
