import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TresRComponent} from "./tres-r.component";
import { BoardComponent } from './board/board.component';
import { SquareComponent } from './square/square.component';



@NgModule({
  declarations: [
    TresRComponent,
    BoardComponent,
    SquareComponent
  ],
  imports: [
    CommonModule
  ],
  exports:[
    TresRComponent
  ]
})
export class TresRModule { }
