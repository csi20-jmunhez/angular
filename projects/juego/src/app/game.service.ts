import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GameService {

  board = <any>[];
  boardSize: number = 9;
  activePlayer: string = "X";
  turnCount!: 0;
  isGameRunning: boolean = false;
  isGameOver: boolean = false;
  winner: boolean = false;

  constructor() {
    this.newGame()
  }

  newGame(){
    this.activePlayer = "X";
    this.turnCount = 0;
    this.isGameRunning = false;
    this.isGameOver =  false;
    this.winner = false;
    this.board = this.createBoard();
  }

  createBoard(){
    let board = [];
    for( let i = 0; i < this.boardSize; i ++ ){
      board.push( { id: i, state: null } )
    };
    return board
  }

  get getBoard (){
    return this.board
  }

  set setBoard( board:any  ){
    this.board = [...board]
  }

  changePlayerTurn( squareClicked:any){
    this.updateBoard( squareClicked )
    if(!this.isGameOver) this.activePlayer = this.activePlayer === "X" ? "O" : "X"
    this.turnCount ++;
    this.isGameOver = this.isGameOver ? true : false;
  }

  updateBoard( squareClicked:any ){
    this.board[ squareClicked.id ].state = squareClicked.state
    if (this.isWinner) {
      this.winner = true;
      this.isGameRunning = false;
      this.isGameOver = true;
    }
  }

  get gameOver(): boolean{
    return this.turnCount > 8 || this.winner ? true : false
  }

  get isWinner(): boolean{
    return this.checkDiag() || this.checkRows(this.board, "row") || this.checkRows(this.board, "col") ? true : false;
  }

  checkRows( board:any, mode:any ): boolean{

    const
      ROW = mode === "row" ? true : false,
      DIST = ROW ? 1 : 3,
      INC = ROW ? 3 : 1,
      NUMTIMES = ROW ? 7 : 3;

    for ( let i = 0; i < NUMTIMES; i += INC ){

      let
        firstSquare = board[i].state,
        secondSquare =  board[i + DIST].state,
        thirdSquare = board[ i + ( DIST * 2)].state;

      if ( firstSquare && secondSquare && thirdSquare  ){
        if ( firstSquare === secondSquare && secondSquare === thirdSquare ) return true
      }
    }
    return false
  }

  checkDiag (){
    const timesRun = 2,
      midSquare = this.board[4].state;

    for( let i = 0; i <= timesRun; i+=2 ){

      let
        upperCorner = this.board[i].state,
        lowerCorner =  this.board[8 - i].state;

      if ( midSquare && upperCorner && lowerCorner  ){
        if( midSquare === upperCorner && upperCorner === lowerCorner) return true
      }
    }

    return false
  }

  //----------------------------------------------------------------------------

  getComputerChoice(): string {
    const choices = ['r', 'p', 's']; // Roca, Pape, Tijeras
    const randomChoice = Math.floor(Math.random() * 3);
    return choices[randomChoice];
  }

  game(
    userChoice: string
  ): {
    message: string;
    userAdd: number;
    compAdd: number;
  } {
    const playUserComp = userChoice + this.getComputerChoice();
    console.log(`Jugada realizada: ${playUserComp}`);
    let playStatus: {
      message: string;
      userAdd: number;
      compAdd: number;
    };
    switch (playUserComp) {
      // Ganamos
      case 'rs':
      case 'sp':
      case 'pr':
        playStatus = {
          message: 'Ganas a la computadora',
          userAdd: 1,
          compAdd: 0,
        };
        break;
      // Gana la computadora
      case 'rp':
      case 'ps':
      case 'sr':
        playStatus = {
          message: 'Gana la computadora',
          userAdd: 0,
          compAdd: 1,
        };
        break;
      // Empatamos
      case 'rr':
      case 'pp':
      case 'ss':
        playStatus = {
          message: 'Habéis elegido la misma jugada y habéis empatado',
          userAdd: 0,
          compAdd: 0,
        };
        break;
    }
    return playStatus!;
  }
}
