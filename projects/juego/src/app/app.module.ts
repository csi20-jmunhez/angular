import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {PptModule} from "./ppt/ppt.module";
import {TresRModule} from "./tres-r/tres-r.module";
import {RoutingModule} from "./routing.module";
import {FormsModule} from "@angular/forms";
import {GameService} from "./game.service";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    PptModule,
    TresRModule,
    RoutingModule
  ],
  providers: [GameService],
  bootstrap: [AppComponent]
})
export class AppModule { }
