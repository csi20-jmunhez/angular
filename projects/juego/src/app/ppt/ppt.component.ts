import { Component, OnInit } from '@angular/core';
import {GameService} from "../game.service";

@Component({
  selector: 'app-ppt',
  templateUrl: './ppt.component.html',
  styleUrls: ['./ppt.component.css']
})
export class PptComponent implements OnInit {

  constructor(private playGame: GameService) {
  }

  result1!: string;
  result2!: string;
  pointsUser = 0;
  pointsComp =  0;

  ngOnInit(): void {
    this.result1 = 'Esperando jugada...';
    this.result2 = 'Selecciona tu jugada';
    console.log(this.pointsUser);
  }

  play(choice: string): void {
    const result = this.playGame.game(choice);
    this.result1 = result.message;
    this.pointsUser += result.userAdd;
    this.pointsComp += result.compAdd;
  }
}
