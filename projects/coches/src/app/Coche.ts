
export interface Coche {
  modelo: string;
  precioBase: number;
  imagen: string;
  opciones: {
    acabado: string;
    color: string;
    motorizacion: string;
  },
  precioFinal: number;
  texto: string;
}
