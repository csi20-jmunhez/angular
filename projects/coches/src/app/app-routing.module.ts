import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {InicioComponent} from "./componentes/inicio/inicio.component";
import {PersonalizacionComponent} from "./componentes/personalizacion/personalizacion.component";

const routes: Routes = [
  { path: "home", component: InicioComponent},
  { path: "opciones/:modelo", component: PersonalizacionComponent},
  { path: "", redirectTo: "home", pathMatch: "full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
