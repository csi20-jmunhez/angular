import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonalizacionComponent} from "../personalizacion/personalizacion.component";
import {InicioComponent} from "./inicio.component";
import {AppRoutingModule} from "../../app-routing.module";



@NgModule({
  declarations: [
    InicioComponent,
    PersonalizacionComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule
  ]
})
export class CocheModule { }
