import { Component, OnInit } from '@angular/core';
import {Coche} from "../../Coche";
import {COCHES} from "../../mock";

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {

  coches:Coche[] = [];

  constructor() { }


  ngOnInit(): void {
    this.coches = COCHES;
  }

}
