import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Coche} from "../../Coche";
import {COCHES} from "../../mock";
import {FuncionService} from "../../funcion.service";

@Component({
  selector: 'app-personalizacion',
  templateUrl: './personalizacion.component.html',
  styleUrls: ['./personalizacion.component.css']
})
export class PersonalizacionComponent implements OnInit {

  coche:any;

  constructor(private route:ActivatedRoute,
              public funciones: FuncionService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe((params: ParamMap) => {
      let modelo = <string>params.get('modelo');
      this.coche = COCHES.find(item => item.modelo === modelo);
    });
  }

}
