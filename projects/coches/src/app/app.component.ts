import { Component } from '@angular/core';
import {Coche} from "./Coche";
import {COCHES} from "./mock";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'coches';
}
