import { Injectable } from '@angular/core';
import {Location} from "@angular/common";
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class FuncionService {

  constructor(
    private firestore: AngularFirestore, private location:Location
  ) {}
  //Crea un nuevo Coche
  public createCoche(data: {nombre: string, url: string}) {
    return this.firestore.collection('cats').add(data);
  }
  //Obtiene un Coche
  public getCoche(documentId: string) {
    return this.firestore.collection('cats').doc(documentId).snapshotChanges();
  }
  //Obtiene todos los Coches
  public getCoches() {
    return this.firestore.collection('cats').snapshotChanges();
  }
  //Actualiza un Coche
  public updateCoche(documentId: string, data: any) {
    return this.firestore.collection('cats').doc(documentId).set(data);
  }
  //Borra un Coche
  public deleteCoche(documentId: string) {
    return this.firestore.collection('cats').doc(documentId).delete();
  }

  goBack(){
    this.location.back();
  }
}
