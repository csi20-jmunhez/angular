import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  public cosas:string[];
  public cosaSelect:string = '';


  constructor() {
    this.cosas = ['hola', 'adios', 'saludo', 'despedida'];
  }

  ngOnInit(): void {
  }

  selectCosa(cosaSeleccionada: string) {
    this.cosaSelect = cosaSeleccionada;
  }

  recibir(event:any){

    let index!:number;
    let i = this.cosas.find(u => u === this.cosaSelect);

    if (i != null) {
      index = this.cosas.indexOf(i);
    }

    this.cosas[index] = event;
  }
}
