import { Component, OnInit } from '@angular/core';
import {FirestoreService} from "../services/firestore.service";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-cats',
  templateUrl: './cats.component.html',
  styleUrls: ['./cats.component.css']
})
export class CatsComponent implements OnInit {

  public cats:any[] = [];

  public documentId:string = '';
  public currentStatus = 1;
  public newCatForm = new FormGroup({
    nombre: new FormControl('', Validators.required),
    url: new FormControl('', Validators.required),
    id: new FormControl('')
  });

  constructor( private afs: FirestoreService ) {
    this.inicioParametros();
  }

  ngOnInit() {
    this.afs.getCats().subscribe((catsSnapshot) => {
      this.cats = [];

      catsSnapshot.forEach((catData: any) => {
        this.cats.push({
          id: catData.payload.doc.id,
          data: catData.payload.doc.data()
        });

      })

    });

  }

  public newCat(form:any, documentId = this.documentId) {
    console.log(`Status: ${this.currentStatus}`);
    if (this.currentStatus == 1) {
      let data = {
        nombre: form.nombre,
        url: form.url
      }
      this.afs.createCat(data).then(() => {
        console.log('Documento creado exitósamente!');
        this.inicioParametros();
      }, (error) => {
        console.error(error);
      });
    } else {
      let data = {
        nombre: form.nombre,
        url: form.url
      }
      this.afs.updateCat(documentId, data).then(() => {
        this.currentStatus = 1;
        this.inicioParametros();
        console.log('Documento editado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }
  }

  public editCat(documentId:any) {
    let editSubscribe = this.afs.getCat(documentId).subscribe((cat:any) => {
      this.currentStatus = 2;
      this.documentId = documentId;
      this.newCatForm.setValue({
        id: documentId,
        nombre: cat.payload.data()['nombre'],
        url: cat.payload.data()['url']
      });
      editSubscribe.unsubscribe();
    });
  }

  public deleteCat(documentId:any) {
    this.afs.deleteCat(documentId).then(() => {
      console.log('Documento eliminado!');
    }, (error) => {
      console.error(error);
    });
  }

  private inicioParametros() {
    this.newCatForm.setValue({
      nombre: '',
      url: '',
      id: ''
    });
  }

}
