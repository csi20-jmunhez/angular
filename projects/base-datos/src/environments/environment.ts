// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAltMjFjuNidVebVo91rMrM8IIp1fgnIgA",
    authDomain: "basedatos-3e26c.firebaseapp.com",
    projectId: "basedatos-3e26c",
    storageBucket: "basedatos-3e26c.appspot.com",
    messagingSenderId: "361568604455",
    appId: "1:361568604455:web:3f36a04ee39e72d8629902"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
