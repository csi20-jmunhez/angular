import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-matematicas',
  templateUrl: './matematicas.component.html',
  styleUrls: ['./matematicas.component.css']
})
export class MatematicasComponent implements OnInit {

  number1 : number = 0;
  number2 : number = 0;
  result1 ?: number;
  result2 ?: number;
  result3 ?: number;
  result4 ?: number;

  suma():number {
    this.number1 = Number(prompt('Numero1: '));
    this.number2 = Number(prompt('Numero2: '));
    return this.result1 = this.number1 + this.number2;
  }

  resta():number {
    this.number1 = Number(prompt('Numero1: '));
    this.number2 = Number(prompt('Numero2: '));
    return this.result2 = this.number1 - this.number2;
  }

  multiplicar():number {
    this.number1 = Number(prompt('Numero1: '));
    this.number2 = Number(prompt('Numero2: '));
    return this.result3 = this.number1 * this.number2;
  }

  dividir():number {
    this.number1 = Number(prompt('Numero1: '));
    this.number2 = Number(prompt('Numero2: '));
    return this.result4 = this.number1 / this.number2;
  }

  ngOnInit(): void {
  }

}
