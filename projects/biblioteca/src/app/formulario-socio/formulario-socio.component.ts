import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {SocioService} from "../socios/socio.service";

@Component({
  selector: 'app-formulario-socio',
  templateUrl: './formulario-socio.component.html',
  styleUrls: ['./formulario-socio.component.css']
})
export class FormularioSocioComponent implements OnInit {

  funcion:string = "Crear";
  public id : any;
  public newSocioForm = new FormGroup({
    Nombre: new FormControl('', Validators.required),
    Apellidos: new FormControl('', Validators.required),
    FechaNac: new FormControl('', Validators.required),
    Dni: new FormControl('', Validators.required),
    Empleo: new FormControl('', Validators.required),
    Img: new FormControl(''),
    Activo: new FormControl(''),
    id: new FormControl('')
  });


  constructor(private socService:SocioService, private route:ActivatedRoute,
              private router:Router){
    this.inicioParametros();
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    if(this.id != undefined){
      this.createSocio(this.newSocioForm);
    }

  }

  public createSocio(form:any, documentId = this.id) {

    if (documentId == undefined) {

      let data = {
        Nombre: form.Nombre,
        Apellidos: form.Apellidos,
        FechaNac: form.FechaNac,
        Dni: form.Dni,
        Empleo:form.Empleo,
        Img: form.Img,
        Activo:true
      }
      this.socService.createSocio(data).then(() => {
        this.inicioParametros();
        console.log('Documento creado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }else {
      this.funcion = 'Editar';

      let editSubscribe = this.socService.getSocio(documentId).subscribe((socio:any) => {
        this.newSocioForm.setValue(
          {
            Nombre: socio.payload.data()['Nombre'],
            Apellidos: socio.payload.data()['Apellidos'],
            FechaNac: socio.payload.data()['FechaNac'],
            Dni: socio.payload.data()['Dni'],
            Empleo: socio.payload.data()['Empleo'],
            Img: socio.payload.data()['Img'],
            Activo:true,
            id: documentId
          }
        );
        console.log(this.newSocioForm);
        editSubscribe.unsubscribe();
      });


      let data = {
        Nombre: form.Nombre,
        Apellidos: form.Apellidos,
        FechaNac: form.FechaNac,
        Dni: form.Dni,
        Empleo:form.Empleo,
        Img: form.Img,
        Activo:true
      }
      console.log(data);

      this.socService.updateSocio(documentId, data).then(() => {
        console.log('Documento editado exitósamente!');
        this.inicioParametros();
      }, (error) => {
        console.error(error);
      });
    }

    alert("Socio Guardado Correctamente");
    this.router.navigateByUrl("/socios");
  }

  private inicioParametros() {
    this.newSocioForm.setValue({
      Nombre: '',
      Apellidos: '',
      FechaNac: '',
      Dni: '',
      Empleo:'',
      Img: '',
      Activo:'',
      id: ''
    });
  }

}
