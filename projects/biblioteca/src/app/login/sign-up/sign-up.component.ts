import { Component, OnInit } from '@angular/core';
import {AuthService} from "../services/auth.service";
import {Location} from "@angular/common";


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  constructor(public authService: AuthService, public location:Location) { }

  ngOnInit(): void {
  }

}
