import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PrestamosComponent} from "./prestamos.component";
import {FormularioPrestamoComponent} from "../formulario-prestamo/formulario-prestamo.component";
import {ReactiveFormsModule} from "@angular/forms";
import {MatCardModule} from "@angular/material/card";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatSelectModule} from "@angular/material/select";
import {PrestamoRoutingModule} from "./prestamo-routing.module";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatInputModule} from "@angular/material/input";
import {MatTableModule} from "@angular/material/table";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatPaginatorModule} from "@angular/material/paginator";



@NgModule({
  declarations: [
    PrestamosComponent,
    FormularioPrestamoComponent
  ],
  imports: [
    CommonModule,
    PrestamoRoutingModule,
    ReactiveFormsModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    MatTableModule,
    MatTooltipModule,
    MatPaginatorModule
  ]
})
export class PrestamoModule { }
