import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class PrestamoService {

  constructor(private firestore: AngularFirestore) {

  }

  public createPrestamo(data : any) {
    return this.firestore.collection('prestamos').add(data);
  }

  public getPrestamo(documentId: string) {
    return this.firestore.collection('prestamos').doc(documentId).snapshotChanges();
  }

  public getPrestamos() {
    return this.firestore.collection('prestamos').snapshotChanges();
  }

  public updatePrestamo(documentId: string, data: any) {
    return this.firestore.collection('prestamos').doc(documentId).set(data);
  }

  public deletePrestamo(documentId: string) {
    return this.firestore.collection('prestamos').doc(documentId).delete();
  }

  public getPrestamosNoDevueltos() {
    return this.firestore.collection('prestamos', ref => ref.where('FechaFin', "==", null)).snapshotChanges();
  }
}
