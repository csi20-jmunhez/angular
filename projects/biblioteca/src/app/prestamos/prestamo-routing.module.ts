import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {PrestamosComponent} from "./prestamos.component";
import {FormularioPrestamoComponent} from "../formulario-prestamo/formulario-prestamo.component";
import {AuthGuard} from "../login/guard/auth.guard";


const route: Routes = [
  { path: "", component: PrestamosComponent, canActivate: [AuthGuard]},
  { path: "prestamonuevo", component: FormularioPrestamoComponent, canActivate: [AuthGuard]},
]

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class PrestamoRoutingModule {
}
