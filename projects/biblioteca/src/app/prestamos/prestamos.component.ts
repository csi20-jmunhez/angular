import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {PrestamoService} from "./prestamo.service";
import {LibroService} from "../libros/libro.service";
import {SocioService} from "../socios/socio.service";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {Router} from "@angular/router";

@Component({
  selector: 'app-prestamos',
  templateUrl: './prestamos.component.html',
  styleUrls: ['./prestamos.component.css']
})
export class PrestamosComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  pageSize:any;
  currentPage:any;

  displayedColumns: string[] = ['FechaInicio', 'Libro', 'Socio', 'Accion'];

  dataSource:any;

  prestamos:any[] = [];
  socios:any[] = [];
  libros:any[] = [];

  constructor(private preService:PrestamoService, private libService:LibroService,
              private socService:SocioService) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.preService.getPrestamosNoDevueltos().subscribe((prestamoSnapshot) => {
      this.prestamos = [];

      prestamoSnapshot.forEach((prestamoData: any) => {
          this.prestamos.push({
            id: prestamoData.payload.doc.id,
            data: prestamoData.payload.doc.data()
          });

          this.dataSource = new MatTableDataSource<any>(this.prestamos);
          this.filterTable();
        }
      )
    });

    console.log(this.prestamos);

    this.libService.getLibros().subscribe((librosSnapshot) => {
          this.libros = [];

          librosSnapshot.forEach((libroData: any) => {
            this.libros.push({
              id: libroData.payload.doc.id,
              data: libroData.payload.doc.data()
            });

          })
          console.log(this.libros);
        });

    this.socService.getSocios().subscribe((sociosSnapshot) => {
      this.socios = [];

      sociosSnapshot.forEach((libroData: any) => {
        this.socios.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data()
        });

      })
      console.log(this.socios);
    });

    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue :any) {
    this.dataSource.filter = filterValue.value.trim().toLowerCase();
  }

  filterTable() {
    this.dataSource.filterPredicate = (data: any, filter: string): boolean => {
      return (this.getLibro(data.data.LibroId).data.Titulo.toLocaleLowerCase().includes(filter)
        || this.getSocio(data.data.SocioId).data.Nombre.toLocaleLowerCase().includes(filter)
      || this.getSocio(data.data.SocioId).data.Apellidos.toLocaleLowerCase().includes(filter))
    }
  }

  pageChanged(event: PageEvent) {
    console.log({ event });
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
  }

  public devolverPrestamos(documentId:any,prestamo:any) {

    let libro = this.libros.find(m => m.id == prestamo.data.LibroId);
    libro.data.Prestado = false;
    prestamo.data.FechaFin = new Date();

    this.preService.updatePrestamo(prestamo.id, prestamo.data);
    this.libService.updateLibro(libro.id, libro.data);

    window.location.reload();
  }

  public getLibro(id:any){

    let libro:any;
    libro = this.libros.find(m => m.id == id);
    console.log(libro);
    return libro;
  }

  public getSocio(id:any){

    let socio:any;
    socio = this.socios.find(m => m.id == id);

    return socio;
  }

}
