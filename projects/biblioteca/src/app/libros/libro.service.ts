import { Injectable } from '@angular/core';
import {AngularFirestore} from "@angular/fire/compat/firestore";

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  constructor(private firestore: AngularFirestore) {

  }

  public createLibro(data : any) {
    return this.firestore.collection('libros').add(data);
  }

  public getLibro(documentId: string) {
    return this.firestore.collection('libros').doc(documentId).snapshotChanges();
  }

  public getLibros() {
    return this.firestore.collection('libros').snapshotChanges();
  }

  public updateLibro(documentId: string, data: any) {
    return this.firestore.collection('libros').doc(documentId).set(data);
  }

  public deleteLibro(documentId: string) {
    return this.firestore.collection('libros').doc(documentId).delete();
  }

  public getLibrosNoPrestado() {
    return this.firestore.collection('libros', ref => ref.where('Prestado', "==", false)).snapshotChanges();
  }

  public getLibrosActivos() {
    return this.firestore.collection('libros', ref => ref.where('Activo', "==", true)).snapshotChanges();
  }

}
