import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LibrosComponent} from "./libros.component";
import {LibroDetailsComponent } from './libro-details/libro-details.component';
import {LibroRoutingModule} from "./libro-routing.module";
import {MatIconModule} from "@angular/material/icon";
import {MatButtonModule} from "@angular/material/button";
import {MatTableModule} from "@angular/material/table";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatCardModule} from "@angular/material/card";
import {FormularioComponent} from "../formulario/formulario.component";
import {ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    LibrosComponent,
    FormularioComponent,
    LibroDetailsComponent
  ],
  imports: [
    CommonModule,
    LibroRoutingModule,
    MatIconModule,
    MatButtonModule,
    MatTableModule,
    MatPaginatorModule,
    MatFormFieldModule,
    MatInputModule,
    MatTooltipModule,
    MatCardModule,
    ReactiveFormsModule
  ]
})
export class LibroModule { }
