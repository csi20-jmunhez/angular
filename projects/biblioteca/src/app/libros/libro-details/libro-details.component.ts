import { Component, OnInit } from '@angular/core';
import {LibroService} from "../libro.service";
import {ActivatedRoute, ParamMap} from "@angular/router";

@Component({
  selector: 'app-libro-details',
  templateUrl: './libro-details.component.html',
  styleUrls: ['./libro-details.component.css']
})
export class LibroDetailsComponent implements OnInit {

  libro:any;

  constructor( private libService:LibroService,
               private route:ActivatedRoute) { }

  ngOnInit(): void {

    let id = this.route.snapshot.params.id;

    this.libro = this.libService.getLibro(id).subscribe((libro:any) => {
      this.libro ={
        id: id,
        Titulo: libro.payload.data()['Titulo'],
        Autor: libro.payload.data()['Autor'],
        Genero: libro.payload.data()['Genero'],
        Precio: libro.payload.data()['Precio'],
        Img: libro.payload.data()['Img']
      };

      console.log(this.libro);
    });
  }


}
