import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {LibroService} from "./libro.service";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator, PageEvent} from "@angular/material/paginator";

@Component({
  selector: 'app-libros',
  templateUrl: './libros.component.html',
  styleUrls: ['./libros.component.css']
})
export class LibrosComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  pageSize:any;
  currentPage:any;

  displayedColumns: string[] = ['Portada', 'Titulo', 'Accion'];

  libro:any;
  libros:any[] = [];

  dataSource:any;

  constructor( private libService:LibroService) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {
    this.libService.getLibrosActivos().subscribe((librosSnapshot) => {
      this.libros = [];

      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data()
        });

      })

      this.dataSource = new MatTableDataSource<any>(this.libros);
      this.filterTable();
    });

    this.dataSource.paginator = this.paginator;
  }

  applyFilter(filterValue :any) {
    this.dataSource.filter = filterValue.value.trim().toLowerCase();
  }

  filterTable() {
    this.dataSource.filterPredicate = (data: any, filter: string): boolean => {
      return (data.data.Titulo.toLocaleLowerCase().includes(filter))
    }
  }

  pageChanged(event: PageEvent) {
    console.log({ event });
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
  }

  public desactivoLibro(libro:any) {
    libro.data.Activo = false;
    libro.data.Prestado = true;
    this.libService.updateLibro(libro.id, libro.data);
  }

}
