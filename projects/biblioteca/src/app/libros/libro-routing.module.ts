import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LibrosComponent} from "./libros.component";
import {LibroDetailsComponent} from "./libro-details/libro-details.component";
import {FormularioComponent} from "../formulario/formulario.component";
import {AuthGuard} from "../login/guard/auth.guard";

const route:Routes = [
  { path:"", component: LibrosComponent, canActivate: [AuthGuard]},
  { path: "libronuevo", component: FormularioComponent, canActivate: [AuthGuard]},
  { path: ":id", component: LibroDetailsComponent}
  ]

@NgModule({
  imports:[RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class LibroRoutingModule { }
