import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {PrestamoService} from "../prestamos/prestamo.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LibroService} from "../libros/libro.service";
import {SocioService} from "../socios/socio.service";

@Component({
  selector: 'app-formulario-prestamo',
  templateUrl: './formulario-prestamo.component.html',
  styleUrls: ['./formulario-prestamo.component.css']
})
export class FormularioPrestamoComponent implements OnInit {

  libros:any[] = [];
  socios:any[] = [];
  public id : any;
  public newPrestamoForm = new FormGroup({
    SocioId: new FormControl('', Validators.required),
    LibroId: new FormControl('', Validators.required),
    FechaInicio: new FormControl(''),
    FechaFin: new FormControl(''),
    id: new FormControl('')
  });

  constructor(private prestamoSer: PrestamoService, private route:ActivatedRoute,
              private router:Router, private libService:LibroService,
              private socService:SocioService) { }

  ngOnInit(): void {
    this.libService.getLibrosNoPrestado().subscribe((librosSnapshot) => {
      this.libros = [];

      librosSnapshot.forEach((libroData: any) => {
        this.libros.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data()
        });

      })
      console.log(this.libros);
    });

    this.socService.getSociosActivos().subscribe((sociosSnapshot) => {
      this.socios = [];

      sociosSnapshot.forEach((libroData: any) => {
        this.socios.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data()
        });

      })
      console.log(this.socios);
    });

    this.id = this.route.snapshot.params.id;

    if(this.id != undefined) {
      this.createPrestamo(this.newPrestamoForm);
    }

  }

  public createPrestamo(form:any, documentId = this.id) {

      let data = {
        SocioId: form.SocioId,
        LibroId: form.LibroId,
        FechaInicio: new Date(),
        FechaFin: null
      }
      this.prestamoSer.createPrestamo(data).then(() => {
        this.inicioParametros();
        console.log('Documento creado exitósamente');
      }, (error) => {
        console.log(error);
      });

      let libro = this.libros.find(m => m.id == data.LibroId);
      libro.data.Prestado = true;
      libro.id = data.LibroId;
      this.libService.updateLibro(libro.id, libro.data);

      alert("Prestamo Guardado Correctamente");
      this.router.navigateByUrl("/prestamos");
  }

  private inicioParametros() {
    this.newPrestamoForm.setValue({
      SocioId: '',
      LibroId: '',
      FechaInicio: '',
      FechaFin: '',
      id: ''
    });
  }

}
