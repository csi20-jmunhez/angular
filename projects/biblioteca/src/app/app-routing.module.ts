import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {FormularioComponent} from "./formulario/formulario.component";
import {FormularioSocioComponent} from "./formulario-socio/formulario-socio.component";
import {SignInComponent} from "./login/sign-in/sign-in.component";
import {SignUpComponent} from "./login/sign-up/sign-up.component";
import {ForgotPasswordComponent} from "./login/forgot-password/forgot-password.component";
import {VerifyEmailComponent} from "./login/verify-email/verify-email.component";
import {DashboardComponent} from "./login/dashboard/dashboard.component";
import {AuthGuard} from "./login/guard/auth.guard";

const routes: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: 'register-user', component: SignUpComponent },
  { path: 'dashboard' , component: DashboardComponent, canActivate: [AuthGuard]}, //
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'verify-email-address', component: VerifyEmailComponent },

  { path: "home", component: HomeComponent},

  { path: "libros", loadChildren: () => import('./libros/libro.module')
      .then(m => m.LibroModule)},
  { path: "libroModificado/:id", component: FormularioComponent},

  { path: "socios", loadChildren: () => import('./socios/socio.module')
      .then(m => m.SocioModule)},
  { path: "socioModificado/:id", component: FormularioSocioComponent},

  { path: "prestamos", loadChildren: () => import('./prestamos/prestamo.module')
      .then(m => m.PrestamoModule)},

  { path: '', redirectTo: '/sign-in', pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
