import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {LibroService} from "../libros/libro.service";
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  funcion:string = "Crear";
  public libro:any;
  public id : any;
  public newLibroForm = new FormGroup({
    Titulo: new FormControl('', Validators.required),
    Autor: new FormControl('', Validators.required),
    Genero: new FormControl('', Validators.required),
    Precio: new FormControl('', Validators.required),
    Img: new FormControl('', Validators.required),
    Prestado: new FormControl(''),
    Activo: new FormControl(''),
    id: new FormControl('')
  });


  constructor(private libService:LibroService, private route:ActivatedRoute,
              private router:Router){}

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;

    if(this.id != undefined) {
      this.createLibro(this.newLibroForm);
    }

  }

  public createLibro(form:any, documentId = this.id) {
    if (documentId == null) {

      let data = {
        Titulo: form.Titulo,
        Autor: form.Autor,
        Genero: form.Genero,
        Precio: form.Precio,
        Img: form.Img,
        Prestado: false,
        Activo: true
      }
      this.libService.createLibro(data).then(() => {
        this.inicioParametros();
        console.log('Documento creado exitósamente');
      }, (error) => {
        console.log(error);
      });
    }else {

      this.funcion = 'Editar';

      let editSubscribe = this.libService.getLibro(documentId).subscribe((libro:any) => {
        this.newLibroForm.setValue(
          {
            Titulo: libro.payload.data()['Titulo'],
            Autor: libro.payload.data()['Autor'],
            Genero: libro.payload.data()['Genero'],
            Precio: libro.payload.data()['Precio'],
            Img: libro.payload.data()['Img'],
            Prestado: libro.payload.data()['Prestado'],
            Activo: libro.payload.data()['Activo'],
            id: documentId
          }
        );
        console.log(this.newLibroForm);
        editSubscribe.unsubscribe();
      });


      let data = {
        Titulo: form.Titulo,
        Autor: form.Autor,
        Genero: form.Genero,
        Precio: form.Precio,
        Img: form.Img,
        Prestado: false,
        Activo: true
      }
      console.log(data);

      this.libService.updateLibro(documentId, data).then(() => {
        console.log('Documento editado exitósamente!');
        this.inicioParametros();
      }, (error) => {
        console.error(error);
      });
    }

    alert("Libro Guardado Correctamente");
    this.router.navigateByUrl("/libros");
  }

  private inicioParametros() {
    this.newLibroForm.setValue({
      Titulo: '',
      Autor: '',
      Genero: '',
      Precio: '',
      Img: '',
      Prestado: '',
      Activo: '',
      id: ''
    });
  }

}
