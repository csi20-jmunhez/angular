import {NgModule} from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {SociosComponent} from "./socios.component";
import {SocioDetailsComponent} from "./socio-details/socio-details.component";
import {FormularioSocioComponent} from "../formulario-socio/formulario-socio.component";
import {AuthGuard} from "../login/guard/auth.guard";

const route: Routes = [
  { path: "", component: SociosComponent, canActivate: [AuthGuard]},
  { path: "socionuevo", component: FormularioSocioComponent, canActivate: [AuthGuard]},
  { path: ":id", component: SocioDetailsComponent }
]

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class SocioRoutingModule {
}
