import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SociosComponent} from "./socios.component";
import { SocioDetailsComponent } from './socio-details/socio-details.component';
import {SocioRoutingModule} from "./socio-routing.module";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatTableModule} from "@angular/material/table";
import {MatButtonModule} from "@angular/material/button";
import {MatInputModule} from "@angular/material/input";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatCardModule} from "@angular/material/card";
import {FormularioSocioComponent} from "../formulario-socio/formulario-socio.component";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {MatRadioModule} from "@angular/material/radio";


@NgModule({
  declarations: [
    SociosComponent,
    FormularioSocioComponent,
    SocioDetailsComponent
  ],
  imports: [
    SocioRoutingModule,

    MatIconModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatRadioModule,
    MatCardModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatTableModule,
    MatTooltipModule,
    CommonModule,
  ]

})
export class SocioModule { }
