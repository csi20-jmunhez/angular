import {AfterViewInit, Component, OnInit, ViewChild} from '@angular/core';
import {SocioService} from "./socio.service";
import {PrestamoService} from "../prestamos/prestamo.service";
import {MatPaginator, PageEvent} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'app-socios',
  templateUrl: './socios.component.html',
  styleUrls: ['./socios.component.css']
})
export class SociosComponent implements OnInit, AfterViewInit {

  @ViewChild(MatPaginator) paginator!: MatPaginator;

  pageSize:any;
  currentPage:any;

  displayedColumns: string[] = ['Rostro', 'Nombre', 'Accion'];

  prestamos:any[] = [];
  socios:any[] = [];

  dataSource:any;

  constructor( private socService:SocioService, private preService:PrestamoService) { }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit(): void {

    this.socService.getSociosActivos().subscribe((sociosSnapshot) => {
      this.socios = [];

      sociosSnapshot.forEach((libroData: any) => {
        this.socios.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data()
        });

      })

      this.dataSource = new MatTableDataSource<any>(this.socios);
      this.filterTable();
    });

    this.preService.getPrestamosNoDevueltos().subscribe((prestamoSnapshot) => {
      this.prestamos = [];

      prestamoSnapshot.forEach((prestamoData: any) => {
          this.prestamos.push({
            id: prestamoData.payload.doc.id,
            data: prestamoData.payload.doc.data()
          });
        }
      )}
    );

    this.dataSource.paginator = this.paginator;

  }
  applyFilter(filterValue :any) {
    this.dataSource.filter = filterValue.value.trim().toLowerCase();
  }

  filterTable() {
    this.dataSource.filterPredicate = (data: any, filter: string): boolean => {
      return (data.data.Nombre.toLocaleLowerCase().includes(filter) || data.data.Apellidos.toLocaleLowerCase().includes(filter))
    }
  }

  pageChanged(event: PageEvent) {
    console.log({ event });
    this.pageSize = event.pageSize;
    this.currentPage = event.pageIndex;
  }


  public bajaSocio(socio:any) {
    socio.data.Activo = false;
    this.socService.updateSocio(socio.id, socio.data);
  }

  public socioPrestamo(id:any){

    let socio = this.prestamos.find(m => m.data.SocioId == id);

    return socio != undefined;

  }

}
