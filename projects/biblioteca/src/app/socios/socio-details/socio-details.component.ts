import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SocioService} from "../socio.service";

@Component({
  selector: 'app-socio-details',
  templateUrl: './socio-details.component.html',
  styleUrls: ['./socio-details.component.css']
})
export class SocioDetailsComponent implements OnInit {

  socio:any;

  constructor( private socService:SocioService,
               private route:ActivatedRoute) { }

  ngOnInit(): void {

    let id = this.route.snapshot.params.id;

    this.socio = this.socService.getSocio(id).subscribe((libro:any) => {
      this.socio = {
        id: id,
        Nombre: libro.payload.data()['Nombre'],
        Apellidos: libro.payload.data()['Apellidos'],
        FechaNac: libro.payload.data()['FechaNac'],
        Dni: libro.payload.data()['Dni'],
        Empleo: libro.payload.data()['Empleo'],
        Img: libro.payload.data()['Img']
      };

      console.log(this.socio);
    });
  }

}
