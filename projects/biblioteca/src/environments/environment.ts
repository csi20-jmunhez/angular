// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyAMZfBHO7ZLrQzRz3IBsRBczUv7gkfasDM",
    authDomain: "biblioteca-4c4f9.firebaseapp.com",
    projectId: "biblioteca-4c4f9",
    storageBucket: "biblioteca-4c4f9.appspot.com",
    messagingSenderId: "917546094202",
    appId: "1:917546094202:web:526f2c89a34f636aeb35c3"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
