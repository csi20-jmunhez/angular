import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LibroListaComponent} from "./libros/libro-lista/libro-lista.component";

const appRoutes: Routes = [
  { path: "libros", loadChildren: () => import('./libros/libros.module')
      .then(m => m.LibrosModule)},
  { path: "autores", loadChildren: () => import('./autores/autores.module')
      .then(m => m.AutoresModule)},
  { path: "", redirectTo: "libros", pathMatch: "full" },
  { path: "**", component: LibroListaComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
