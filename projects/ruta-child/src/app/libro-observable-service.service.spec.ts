import { TestBed } from '@angular/core/testing';

import { LibroObservableServiceService } from './libro-observable-service.service';

describe('LibroObservableServiceService', () => {
  let service: LibroObservableServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LibroObservableServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
