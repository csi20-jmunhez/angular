import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './componentes/home/home.component';
import {LibrosModule} from "./libros/libros.module";
import {AutoresModule} from "./autores/autores.module";

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LibrosModule,
    AutoresModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
