import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {AutorListaComponent} from "./autor-lista/autor-lista.component";

const route:Routes = [
  { path: "", component: AutorListaComponent }
]

@NgModule({
  imports: [RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class AutorRoutingModule { }
