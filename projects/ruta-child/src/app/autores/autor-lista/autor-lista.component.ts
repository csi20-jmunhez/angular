import { Component, OnInit } from '@angular/core';
import {LIBROS} from "../../mocks";
import {Libro} from "../../libro.model";

@Component({
  selector: 'app-autor-lista',
  templateUrl: './autor-lista.component.html',
  styleUrls: ['./autor-lista.component.css']
})
export class AutorListaComponent implements OnInit {

  Libros:Libro[] = [];

  constructor() { }

  ngOnInit(): void {
    this.Libros = LIBROS;
  }

}
