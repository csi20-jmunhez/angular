import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AutorListaComponent} from "./autor-lista/autor-lista.component";
import {AutorRoutingModule} from "./autor-routing.module";



@NgModule({
  declarations: [
    AutorListaComponent
  ],
  imports: [
    CommonModule,
    AutorRoutingModule
  ]
})
export class AutoresModule { }
