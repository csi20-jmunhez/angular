import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";

@Component({
  selector: 'app-libro-opiniones',
  templateUrl: './libro-opiniones.component.html',
  styleUrls: ['./libro-opiniones.component.css']
})
export class LibroOpinionesComponent implements OnInit {

  idLibro?: number;

  constructor( private route: ActivatedRoute ) { }
  ngOnInit() {
    this.route.parent?.paramMap.subscribe((params: ParamMap) => {
      this.idLibro = parseInt(<string>params.get('id'));
    });
  }

}
