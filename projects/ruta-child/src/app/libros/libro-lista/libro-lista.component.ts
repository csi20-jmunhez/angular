import { Component, OnInit } from '@angular/core';
import {Libro} from "../../libro.model";
import {LibroObservableServiceService} from "../../libro-observable-service.service";
import {Subscription} from "rxjs";

@Component({
  selector: 'app-libro-lista',
  templateUrl: './libro-lista.component.html',
  styleUrls: ['./libro-lista.component.css']
})
export class LibroListaComponent implements OnInit {

  libros: Libro[] = [];
  observableSubs! : Subscription;

  constructor(private libroService: LibroObservableServiceService) { }

  ngOnInit(): void {
    this.observableSubs = this.libroService.getLibros()
      .subscribe(
        libros => this.libros = libros,
        error => console.log(error),
        () => console.log("this.libroObservableService.getLibros() FINALIZADO"));
  }

  ngOnDestroy(): void {
    if (this.observableSubs)
      this.observableSubs.unsubscribe();
  }

}
