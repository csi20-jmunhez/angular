import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, ParamMap} from "@angular/router";
import {Libro} from "../../libro.model";
import {LIBROS} from "../../mocks";

@Component({
  selector: 'app-libro-imagenes',
  templateUrl: './libro-imagenes.component.html',
  styleUrls: ['./libro-imagenes.component.css']
})
export class LibroImagenesComponent implements OnInit {

  Libro?:Libro;
  idLibro: number = 0;

  constructor( private route: ActivatedRoute ) { }
  ngOnInit() {
    this.route.parent?.paramMap.subscribe((params: ParamMap) => {
        this.idLibro = parseInt(<string>params.get('id'));
      });

    this.Libro = this.getLibro(this.idLibro);

    console.log(this.Libro);
  }

  getLibro(id:number):any{
    return this.Libro = LIBROS.find(item => item.id === id);
  }

}
