import { NgModule } from '@angular/core';
import {RouterModule, Routes} from "@angular/router";
import {LibroDetalleComponent} from "./libro-detalle/libro-detalle.component";
import {LibroImagenesComponent} from "./libro-imagenes/libro-imagenes.component";
import {LibroOpinionesComponent} from "./libro-opiniones/libro-opiniones.component";
import {LibroListaComponent} from "./libro-lista/libro-lista.component";

const route:Routes = [
  { path:"", component: LibroListaComponent},
  { path: "libros/:id", component: LibroDetalleComponent,
    children: [
      { path: 'imagenes', component: LibroImagenesComponent},
      { path: 'opiniones', component: LibroOpinionesComponent},
      { path: '', redirectTo: "imagenes", pathMatch: "full"},
      { path: "**", component: LibroDetalleComponent }
    ]}
]

@NgModule({

  imports:[RouterModule.forChild(route)],
  exports: [RouterModule]
})
export class LibroRoutingModule { }
