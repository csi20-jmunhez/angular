import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {LibroDetalleComponent} from "./libro-detalle/libro-detalle.component";
import {LibroListaComponent} from "./libro-lista/libro-lista.component";
import {LibroOpinionesComponent} from "./libro-opiniones/libro-opiniones.component";
import {LibroImagenesComponent} from "./libro-imagenes/libro-imagenes.component";
import {LibroRoutingModule} from "./libro-routing.module";


@NgModule({
  declarations: [
    LibroDetalleComponent,
    LibroListaComponent,
    LibroOpinionesComponent,
    LibroImagenesComponent
  ],
  imports: [
    CommonModule,
    LibroRoutingModule
  ]
})
export class LibrosModule { }
