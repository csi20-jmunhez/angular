import {Libro} from "./libro.model";

export const LIBROS: Libro[] = [
  {
    id: 1,
    titulo: "El Quijote",
    autor: "Cervantes",
    img: "assets/quijote.jpg"
  },

  {
    id: 2,
    titulo: "Hamlet",
    autor: "Shakespeare",
    img: "assets/hamlet.jpg"
  }];
